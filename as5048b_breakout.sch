EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:as5048b
LIBS:as5048b_breakout-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AS5048B U1
U 1 1 5A20636C
P 5250 3000
F 0 "U1" H 5250 2350 60  0000 C CNN
F 1 "AS5048B" H 5250 2450 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 5050 3000 60  0001 C CNN
F 3 "" H 5050 3000 60  0001 C CNN
	1    5250 3000
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5A2063C9
P 6400 2950
F 0 "C2" H 6425 3050 50  0000 L CNN
F 1 "10u" H 6425 2850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6438 2800 50  0001 C CNN
F 3 "" H 6400 2950 50  0001 C CNN
	1    6400 2950
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5A20681B
P 4100 3200
F 0 "R1" V 4180 3200 50  0000 C CNN
F 1 "R" V 4100 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4030 3200 50  0001 C CNN
F 3 "" H 4100 3200 50  0001 C CNN
	1    4100 3200
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 5A206842
P 4450 3200
F 0 "R5" V 4530 3200 50  0000 C CNN
F 1 "R" V 4450 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4380 3200 50  0001 C CNN
F 3 "" H 4450 3200 50  0001 C CNN
	1    4450 3200
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 5A206865
P 4100 3450
F 0 "R2" V 4180 3450 50  0000 C CNN
F 1 "R" V 4100 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4030 3450 50  0001 C CNN
F 3 "" H 4100 3450 50  0001 C CNN
	1    4100 3450
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5A20688E
P 4450 3450
F 0 "R6" V 4530 3450 50  0000 C CNN
F 1 "R" V 4450 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4380 3450 50  0001 C CNN
F 3 "" H 4450 3450 50  0001 C CNN
	1    4450 3450
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR01
U 1 1 5A206932
P 5950 2550
F 0 "#PWR01" H 5950 2400 50  0001 C CNN
F 1 "VCC" H 5950 2700 50  0000 C CNN
F 2 "" H 5950 2550 50  0001 C CNN
F 3 "" H 5950 2550 50  0001 C CNN
	1    5950 2550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5A206956
P 6700 2800
F 0 "#PWR02" H 6700 2550 50  0001 C CNN
F 1 "GND" H 6700 2650 50  0000 C CNN
F 2 "" H 6700 2800 50  0001 C CNN
F 3 "" H 6700 2800 50  0001 C CNN
	1    6700 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5A20697A
P 4600 3550
F 0 "#PWR03" H 4600 3300 50  0001 C CNN
F 1 "GND" H 4600 3400 50  0000 C CNN
F 2 "" H 4600 3550 50  0001 C CNN
F 3 "" H 4600 3550 50  0001 C CNN
	1    4600 3550
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 5A20699E
P 3900 3100
F 0 "#PWR04" H 3900 2950 50  0001 C CNN
F 1 "VCC" H 3900 3250 50  0000 C CNN
F 2 "" H 3900 3100 50  0001 C CNN
F 3 "" H 3900 3100 50  0001 C CNN
	1    3900 3100
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5A2069E6
P 6100 2950
F 0 "C1" H 6125 3050 50  0000 L CNN
F 1 "10u" H 6125 2850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6138 2800 50  0001 C CNN
F 3 "" H 6100 2950 50  0001 C CNN
	1    6100 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3000 5950 3000
Wire Wire Line
	5950 2550 5950 3100
Wire Wire Line
	5800 2800 6700 2800
Connection ~ 6100 2800
Wire Wire Line
	5950 3100 6100 3100
Connection ~ 5950 3000
Wire Wire Line
	6400 3100 6400 3150
Wire Wire Line
	6400 3150 5800 3150
Wire Wire Line
	5800 3150 5800 2900
Connection ~ 6400 2800
Wire Wire Line
	3900 3100 3900 3450
Wire Wire Line
	3900 3200 3950 3200
Wire Wire Line
	3900 3450 3950 3450
Connection ~ 3900 3200
Wire Wire Line
	4600 3200 4600 3550
Connection ~ 4600 3450
Wire Wire Line
	4250 3200 4300 3200
Wire Wire Line
	4250 3450 4300 3450
Wire Wire Line
	4300 3200 4300 3000
Wire Wire Line
	4300 3000 4700 3000
Wire Wire Line
	4700 2900 4250 2900
Wire Wire Line
	4250 2900 4250 3450
Wire Wire Line
	4600 3450 5500 3450
Connection ~ 5100 3450
Connection ~ 5200 3450
Connection ~ 5300 3450
Connection ~ 5400 3450
Connection ~ 5000 3450
$Comp
L CONN_01X04 J1
U 1 1 5A20745C
P 3450 2750
F 0 "J1" H 3450 3000 50  0000 C CNN
F 1 "CONN_01X04" V 3550 2750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 3450 2750 50  0001 C CNN
F 3 "" H 3450 2750 50  0001 C CNN
	1    3450 2750
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR06
U 1 1 5A20756C
P 3650 2800
F 0 "#PWR06" H 3650 2550 50  0001 C CNN
F 1 "GND" H 3650 2650 50  0000 C CNN
F 2 "" H 3650 2800 50  0001 C CNN
F 3 "" H 3650 2800 50  0001 C CNN
	1    3650 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2700 4700 2700
Wire Wire Line
	4200 2800 4700 2800
Wire Wire Line
	4200 2900 3650 2900
$Comp
L VCC #PWR07
U 1 1 5A207594
P 3650 2600
F 0 "#PWR07" H 3650 2450 50  0001 C CNN
F 1 "VCC" H 3650 2750 50  0000 C CNN
F 2 "" H 3650 2600 50  0001 C CNN
F 3 "" H 3650 2600 50  0001 C CNN
	1    3650 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2900 4200 2800
$EndSCHEMATC
